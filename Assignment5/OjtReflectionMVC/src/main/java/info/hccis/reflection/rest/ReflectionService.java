package info.hccis.reflection.rest;

import com.google.gson.Gson;
import info.hccis.ojt.dao.OjtReflectionDAO;
import info.hccis.ojt.entity.OjtReflection;

import java.util.ArrayList;
import java.util.Iterator;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("/ReflectionService")
public class ReflectionService {
//
//    @Resource
//    private final CamperRepository cr;
//
//    /**
//     * Note that dependency injection (constructor) is used here to provide the
//     * CamperRepository object for use in this class.
//     *
//     * @param servletContext Context
//     * @since 20181109
//     * @author BJM
//     */
//    public ReflectionService(@Context ServletContext servletContext) {
//        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
//        this.cr = applicationContext.getBean(CamperRepository.class);
//    }

    /**
     * This rest service will provide all users from the associated database.
     *
     * @return json string containing all user information.
     * @since 20181109
     * @author BJM
     */
    @GET
    @Path("/reflection")
    @Produces(MediaType.APPLICATION_JSON)
    public static  Response getUsers() {
        ArrayList<OjtReflection> numberofReflections = (ArrayList<OjtReflection>) OjtReflectionDAO.selectAll();
        Gson gson = new Gson();
        int statusCode = 200;

        if (numberofReflections.isEmpty()) {
            statusCode = 204;
        }

        String temp = "";
        temp = gson.toJson(numberofReflections);
        return Response.status(statusCode)
                .entity(temp).header("Access-Control-Allow-Origin", "*")
                .header("Access-Control-Allow-Methods", "GET, POST, DELETE, PUT").build();

    }

}
