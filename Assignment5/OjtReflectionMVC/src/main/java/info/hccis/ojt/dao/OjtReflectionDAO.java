/**
 * Name:Charles Aondo
 * Date:2018-10-30
 * Purpose: The doa file connects to the database, and enable selection to and fro the database
 */
package info.hccis.ojt.dao;
//import info.hccis.ojt.entity.Camper;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;
import info.hccis.ojt.entity.OjtReflection;

/**
 * This class will contain db functionality for working with student studentReflect
 *
 * @author bjmaclean modified by Charles Aondo
 * @since 20180923
 */
public class OjtReflectionDAO {

    private final static Logger LOGGER = Logger.getLogger(OjtReflectionDAO.class.getName());
    
    public static OjtReflection select(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        OjtReflection theStudent = null;
        try {
            conn = ConnectionUtils.getConnection();
            System.out.println("Loading for: " + idIn);
            sql = "SELECT * FROM ojtReflection where id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {
       
                
                int id = rs.getInt("id");
                int studentId = rs.getInt("studentId");
                String name = rs.getString("name");
                String reflection = rs.getString("reflection");
//                String dob = rs.getString("dob");
                theStudent = new OjtReflection(id,studentId, name, reflection);
                theStudent.setReflection(reflection);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return theStudent;
    }

    /**
     * Select all studentReflect
     *
     * @author bjmaclean modified by Charles Aondo
     * @since 20160929
     * @return
     */
    public static ArrayList<OjtReflection> selectAll() {
        ArrayList<OjtReflection> studentReflect = new ArrayList();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM ojtReflection order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");                    
                int studentId = rs.getInt("studentId");
                String name = rs.getString("name");
                String reflection = rs.getString("reflection");
                OjtReflection studentReflections = new OjtReflection(id, studentId, name,reflection);
                studentReflect.add(studentReflections);
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return studentReflect;
    }
    /**
     * This method will delete a studentReflections based on the id passed in.
     *
     * @author BJ modified by Charles Aondo
     * @since 20181023
     */
    public static synchronized void delete(int id) throws Exception {
//        System.out.println("inserting studentReflections");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            sql = "DELETE FROM `ojtreflection` WHERE id=?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, id);

            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }

    }
    /**
     * This method will insert.
     *
     * @return
     * @author BJ modified by Charles Aondo
     * @since 20181023
     */
 public static synchronized OjtReflection update(OjtReflection reflection) throws Exception {
//        System.out.println("inserting reflection");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;


        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            //Check to see if reflection exists.
            //Note that the default for an Integer is null not 0  !!!
            //When I try to compare the getId() to null it throws an exception.
            
            int camperIdInt;
            try {
                camperIdInt = reflection.getId();
            } catch (Exception e) {
                camperIdInt = 0;

            }

            if (camperIdInt == 0) {

                sql = "SELECT max(id) from ojtreflection";
                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                int max = 0;
                while (rs.next()) {
                    max = rs.getInt(1) + 1;
                }

                reflection.setId(max);

                sql = "INSERT INTO `ojtreflection`(`id`, `studentId`, `name`, `reflection`) "
                        + "VALUES (?,?,?,?)";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, reflection.getId());
                ps.setInt(2, reflection.getStudentId());
                ps.setString(3, reflection.getName());
                ps.setString(4, reflection.getReflection());
            } else {

                sql = "UPDATE `ojtreflection` SET `studentId`=?,`name`=?,`reflection`=? WHERE id = ?";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, reflection.getStudentId());
                ps.setString(2, reflection.getName());
                ps.setString(3, reflection.getReflection());
                ps.setInt(4, reflection.getId());

            }
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */
            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
        return reflection;
    }
}
