/*
@author: Charles Aondo
@Since:2018-10-01
@purpose:This is the main class for the ojt reflection, this class allows an ojt student to 
update their reflections while the are on the job training
 */
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

public class OjtReflectionMain {

    public static String MENU = "Menu :\nA) Add a Reflection\nS)Show students Relflections \nU)Update Reflection \nX) Exit";
    public static void main(String[] args) throws IOException {
        ArrayList<OjtReflection> studentsReflections = new ArrayList();
        ojtReflectionDAO dataBaseReflection = new ojtReflectionDAO();
        String option;
        do {
            System.out.println(MENU);
            option = FileUtility.getInput().nextLine().toUpperCase();
            String select;
            switch (option) {
                case "A":
                    OjtReflection reflect = new OjtReflection(true);
                    dataBaseReflection.addReflection(reflect);
                    break;
                case "S":
                    System.out.println("Displaying OJT students and their Reflections");
                    studentsReflections = dataBaseReflection.selectAll();
                    for (OjtReflection showStudents : studentsReflections) {
                        System.out.println(showStudents);
                    }
                    break;
                case "U":
                    System.out.println("Enter your student id");
                    int studentId = FileUtility.getInput().nextInt();
                    FileUtility.getInput().nextLine(); //burn the line
                    OjtReflection studentUpdate = dataBaseReflection.select(studentId);
             
                    if(studentUpdate == null){
                        System.out.println("OJT student not found");
                    }else{
                        studentUpdate.addNewReflection();
                        dataBaseReflection.updateReflections(studentUpdate);
                        System.out.println("Reflection Updated");
                    }
                    break;
                default:
                    System.out.println("Bye");
            }

        } while (!option.equalsIgnoreCase("x"));
    }
}
