
/**
 * @author aondo
 * @since  2018-10-12
 * @purpose This class stores the students attributes and reflections to database, retrieve it for editing,formatting and display to the user
 * 
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
public class ojtReflectionDAO {

    Connection conn = null;
    Statement stmt;
    
    public ojtReflectionDAO(){
              try {
            conn = DriverManager.getConnection(
                    "jdbc:mysql://localhost:3306/ojt",
                    "cis2232_admin",
                    "Test1234");

            stmt = conn.createStatement();
        } catch (SQLException sqle) {
            System.out.println("sql exception caught");
            sqle.printStackTrace();
        }
    }
     public void addReflection(OjtReflection reflections){
         
         try{
            String query = "INSERT INTO `ojtreflection`(`studentid`, `name`, `reflection`) VALUES (";
            query += reflections.getStudentID() + ",'" + reflections.getStudentName()+ "','" + reflections.getStudentReflection() + "'";
            query += ");";
            System.out.println("About to query:" + query);
            stmt.executeUpdate(query);
         }catch(SQLException sqle){
             System.out.println(sqle.getMessage());
             System.out.println("Reflection not added");
         }
     }
   public ArrayList<OjtReflection> selectAll(){
         ArrayList<OjtReflection> reflections;
                try {
            //Next select all the rows and display them here...
            reflections = new ArrayList();

            ResultSet rs = stmt.executeQuery("select * from ojtreflection");
            while (rs.next()) {
                OjtReflection reflection = new OjtReflection();
                reflection.setStudentID(rs.getInt("studentID"));
                reflection.setStudentName(rs.getString(2));
                reflection.setStudentReflection(rs.getString(3));
                reflections.add(reflection);
            }

            return reflections;
        } catch (SQLException ex) {
            Logger.getLogger(ojtReflectionDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
   }
   
    /**
     * This method will return a particular camper.
     *
     * @since 20180928
     * @author  Charles Aondo
     */
    public OjtReflection select(int studentId) {

        try {
            PreparedStatement theStatement = null;
            String sqlString = "select * from ojtreflection where studentid=?";
            theStatement = conn.prepareStatement(sqlString);
            theStatement.setInt(1, studentId);
            ResultSet rs = theStatement.executeQuery();             
            OjtReflection reflection = null;
   
            while (rs.next()) {
                reflection = new OjtReflection();
                reflection.setStudentID(rs.getInt("studentId"));
                reflection.setStudentName(rs.getString(2));
                reflection.setStudentReflection(rs.getString(3));
            }

            
            return reflection;
        } catch (SQLException ex) {
            Logger.getLogger(ojtReflectionDAO.class.getName()).log(Level.SEVERE, null, ex);
            return null;
        }
    }

 public void updateReflections(OjtReflection reflect){
          try {

            String query = "UPDATE ojtreflection SET "
                    + "studentid='"+reflect.getStudentID()+"',name='"+
                    reflect.getStudentName()+"',reflection='"+reflect.getStudentReflection()
                    +"' WHERE studentid="+reflect.getStudentID();
            System.out.println("About to query:" + query);
            stmt.executeUpdate(query);

        } catch (SQLException sqle) {
            System.out.println("Could not update the database");
        }
    }
 }

