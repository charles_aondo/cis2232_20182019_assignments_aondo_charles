/*
name: Charles Aondo
date:2018-10-10
purpose:This file creates the database table using the mysql as the database source
*/
create database ojt;
use ojt;

/*create a user in database*/
grant select, insert, update, delete on ojt.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;

--
-- Table structure for table `player`
--

CREATE TABLE IF NOT EXISTS OjtReflection (

studentId int(5) NOT NULL,

name varchar(20) NOT NULL,
reflection varchar(140) NOT NULL
);