
import com.google.gson.Gson;
import java.io.Serializable;
import java.util.Scanner;

/**
 *
 * @author aondo charles
 * @since 2018-10-01
 * @purpose: This class contain the object and its attributes that are used in
 * the ojt reflection main class
 */
public class OjtReflection implements Serializable {

    private String studentName;
    private int studentID;
    private String studentReflection;
    private static int maxRegistrationId;

    public OjtReflection() {

    }

    public String getStudentName() {
        return studentName;
    }

    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }

    public int getStudentID() {
        return studentID;
    }

    public void setStudentID(int studentID) {
        this.studentID = studentID;
    }

    public String getStudentReflection() {
        return studentReflection;
    }

    public void setStudentReflection(String studentReflection) {
        this.studentReflection = studentReflection;
    }

    public OjtReflection(int id, String name, String reflection) {

    }

    public OjtReflection(String[] parts) {

        this(Integer.parseInt(parts[0]), parts[1], parts[2]);

        if (Integer.parseInt(parts[0]) > maxRegistrationId) {
            maxRegistrationId = Integer.parseInt(parts[0]);
        }
    }

    public OjtReflection(boolean getUser) {
        System.out.println("Enter your name");
        this.studentName = FileUtility.getInput().nextLine();
        System.out.println("What is your student ID");
        this.studentID = FileUtility.getInput().nextInt();
        FileUtility.getInput().nextLine();
        System.out.println("How was your OJT reflection experience");
        this.studentReflection = FileUtility.getInput().nextLine();
    }

    /**
     * constructor which will create from String array
     *
     * @since 20180920
     * @author Charles Aondo
     */
    public String getCSV() {
        return studentID + "," + studentName + "," + studentReflection;
    }

    /**
     * @since 20190920
     * @author Charles Aondo
     */
    /*
    This method askn the student for their new reflection
     */
    public void addNewReflection() {
        System.out.println("Enter the student new reflection");
        this.studentReflection = FileUtility.getInput().nextLine();
    }

    public String getCSV(boolean withLineFeed) {
        if (withLineFeed) {
            return getCSV() + System.lineSeparator();
        } else {
            return getCSV();
        }
    }

    public String getJson() {
        Gson gson = new Gson();
        return gson.toJson(this);
    }

    @Override
    public String toString() {
        return " Student ID: " + studentID + ", Student Name: " + studentName + ", OJT Relection: " + studentReflection;
    }

}
