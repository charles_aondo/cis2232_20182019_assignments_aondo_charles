package info.hccis.camper.main;

import info.hccis.camper.model.jpa.Camper;
import info.hccis.camper.model.jpa.OjtReflection;
import info.hccis.camper.util.UtilityRest;
import java.util.Scanner;
import org.json.JSONArray;
import org.json.JSONObject;

public class TestRestUsingJSONArrayAndJSONObject {

    final public static String MENU = "\nMain Menu \nA) Add Reflection (using CamperServiceJpa !!!)\n"
            + "V) View Reflectionss\n"
            + "X) eXit";
    final static Scanner input = new Scanner(System.in);
    private static final String URL_STRING = "http://localhost:8080/reflection/rest/ReflectionService/reflection";

    public static void main(String[] args) {
        boolean endProgram = false;
        do {
            System.out.println(MENU);
            String choice = input.nextLine();

            switch (choice.toUpperCase()) {
                case "A":
                    //NOTE that the post service has been implemented in the CamperServiceJpa (not CamperService)
                    UtilityRest.addCamperUsingRest(URL_STRING, createReflection());
                    break;
                case "V":
                    String jsonReturned = UtilityRest.getJsonFromRest(URL_STRING);
                    //**************************************************************
                    //Based on the json string passed back, loop through each json
                    //object which is a json string in an array of json strings.
                    //*************************************************************
                    JSONArray jsonArray = new JSONArray(jsonReturned);
                    //**************************************************************
                    //For each json object in the array, show the first and last names
                    //**************************************************************
                    System.out.println("Here are the reflections");
                    for (int currentIndex = 0; currentIndex < jsonArray.length(); currentIndex++) {
                        JSONObject reflectionJson = jsonArray.getJSONObject(currentIndex);
                        System.out.println(reflectionJson.getInt("studentId") + " " + reflectionJson.getString("name")+" "+reflectionJson.getString("reflection"));

                    }
                    break;

                case "X":
                    endProgram = true;
                    break;
                default:
                    System.out.println("INVALID OPTION");
            }
        } while (!endProgram);
    }

    /**
     * Create a camper object by passing asking user for input.
     * @return camper
     * @since 20171117
     * @author BJM
     */
    public static OjtReflection createReflection() {
        OjtReflection newReflection = new OjtReflection();
        
        System.out.println("Enter student Id:");
        newReflection.setStudentId(input.nextInt());
        input.nextLine();
        System.out.println("Enter name:");
        newReflection.setName(input.nextLine());
        
        System.out.println("Enter reflection:");
        newReflection.setReflection(input.nextLine());

//        newReflection.setId(2);
        newReflection.setId(20);

        return newReflection;
    }

}
