/**
 * name:Charles Aondo
 * Date: 2018-10-31
 * Purpose: The controller class handles all the business of the project  
 */
package info.hccis.ojt.web;
import info.hccis.ojt.dao.OjtReflectionDAO;
import info.hccis.ojt.entity.OjtReflection;
import info.hccis.util.Utility;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CamperController {

    @RequestMapping("/ojt/add")
    public String addReflection(Model model) {
        OjtReflection newReflections = new OjtReflection();
        model.addAttribute("ojtReflection", newReflections);
        return "ojt/add";
    }
    @RequestMapping("/ojt/update")
    public String reflectionrUpdate(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
        //- It will go to the database and load the camper details into a camper
        //  object and put that object in the model.  
        OjtReflection editCamper = OjtReflectionDAO.select(Integer.parseInt(idToFind));
        model.addAttribute("ojtReflection", editCamper);
        return "ojt/add";

    }

    @RequestMapping("/ojt/delete")
    public String reflectionDelete(Model model, HttpServletRequest request) {

        //- This method will use the registration id which is passed as a request 
        //parameter.  
        String idToFind = request.getParameter("id");
        System.out.println("BJM id passed=" + idToFind);
        try {
            //- It will go to the database and load the camper details into a camper
            //  object and put that object in the model.
            OjtReflectionDAO.delete(Integer.parseInt(idToFind));
        } catch (Exception ex) {
            System.out.println("Could not delete");
        }
        //Reload the campers list so it can be shown on the next view.
        model.addAttribute("studentReflect",OjtReflectionDAO.selectAll());
        return "/ojt/list";

    }
    @RequestMapping("/ojt/addSubmit")
    public String reflectionAddSubmit(Model model, @Valid @ModelAttribute("ojtReflection") OjtReflection theCamperFromTheForm, BindingResult result) {

        if (Utility.TESTING) {
            System.out.println("BJM-Checking validation." + result.getErrorCount());
        }
        
        if (result.hasErrors()) {
            System.out.println("Error in validation.");
            return "/ojt/add";
        }

        //Call the dao method to put this guy in the database.
        try {
            OjtReflectionDAO.update(theCamperFromTheForm);
        } catch (Exception e) {
            System.out.println("Could not save to the database");
        }

        System.out.println("BJM-Did we get here?");
        //Reload the campers list so it can be shown on the next view.
        model.addAttribute("studentReflect", OjtReflectionDAO.selectAll());
        //This will send the user to the welcome.html page.
        return "ojt/list";
    }

}
