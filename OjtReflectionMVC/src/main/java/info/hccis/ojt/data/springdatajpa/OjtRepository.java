/**
 * @author:Charles Aondo
 * @since:2018-11-14
 * @purpose:This repository provides the function of the spring jpa
 */
package info.hccis.ojt.data.springdatajpa;
import java.io.Serializable;
import java.util.List;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import info.hccis.ojt.entity.OjtReflection;
/**
 *
 * @author aondo
 */
public interface OjtRepository extends CrudRepository<OjtReflection, Integer>{
   
}
