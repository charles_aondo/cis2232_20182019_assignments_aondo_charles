
/*
name: Charles Aondo
date:2018-10-23
purpose:This file creates the database table using the mysql as the database source
-- */
-- Database: `ojt`
-- --
-- 
-- drop database ojt;

create database ojt;

use ojt;
/*create a user in database*/
grant select, insert, update, delete, alter on ojt.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;

CREATE TABLE IF NOT EXISTS OjtReflection ( 

id int(5) NOT NULL PRIMARY KEY AUTO_INCREMENT, 

studentId int(5) NOT NULL, 

name varchar(20) NOT NULL, 

reflection varchar(140) NOT NULL )

